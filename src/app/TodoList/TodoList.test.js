import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import axios from 'axios'
import TodoList from 'app/TodoList'

const db = require('../../../db.json')

const TASKS_DATA = db.tasks

jest.mock('axios', () => ({
  get: jest.fn(() => Promise.resolve({ data: [] })),
  post: jest.fn(() => Promise.resolve({ data: [] })),
  delete: jest.fn(() => Promise.resolve({ data: [] })),
  patch: jest.fn(() => Promise.resolve({ data: [] })),
}))

const createTestQueryClient = () =>
  new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      },
    },
  })

export function renderWithReactQueryClient(ui) {
  const testQueryClient = createTestQueryClient()
  const { rerender, ...result } = render(
    <QueryClientProvider client={testQueryClient}>{ui}</QueryClientProvider>,
  )
  return {
    ...result,
    rerender: (rerenderUi) =>
      rerender(
        <QueryClientProvider client={testQueryClient}>
          {rerenderUi}
        </QueryClientProvider>,
      ),
  }
}

afterEach(() => {
  jest.clearAllMocks()
})

test('renders TodoList', async () => {
  // mock response data
  axios.get.mockResolvedValueOnce({
    data: TASKS_DATA,
  })

  renderWithReactQueryClient(<TodoList />)

  // first render should show shimmer
  expect(screen.getByText(/Awesome/i)).toBeInTheDocument()
  expect(screen.getByTestId('loading')).toBeInTheDocument()

  // when data is loaded, shimmer should be gone
  expect(await screen.findAllByRole('checkbox')).toHaveLength(TASKS_DATA.length)
  expect(screen.queryByTestId('loading')).toBeFalsy()

  const firstTask = TASKS_DATA[0]
  const firstTaskCheckbox = screen.getByLabelText(firstTask.text)
  expect(firstTaskCheckbox).toBeInTheDocument()
  expect(firstTaskCheckbox).toBeChecked()

  // toggle first task
  userEvent.click(firstTaskCheckbox)
  expect(firstTaskCheckbox).not.toBeChecked()
})

test('add new todo to page', async () => {
  // mock response data
  axios.get.mockResolvedValueOnce({
    data: TASKS_DATA,
  })
  axios.post.mockResolvedValueOnce({
    data: {
      id: 6,
      title: 'New Todo',
      completed: false,
    },
  })

  renderWithReactQueryClient(<TodoList />)

  // wait for data to load
  expect(await screen.findAllByRole('checkbox')).toHaveLength(TASKS_DATA.length)

  // add new todo
  const newTodo = 'New Todo'
  const input = screen.getByPlaceholderText(/Add new todo/i)
  const button = screen.getByRole('button', { name: /Submit/i })

  await userEvent.type(input, newTodo)
  await userEvent.click(button)
})

test('delete todo from page', async () => {
  // mock response data
  axios.get.mockResolvedValueOnce({
    data: TASKS_DATA,
  })
  axios.delete.mockResolvedValueOnce({
    data: {
      id: 1,
    },
  })

  renderWithReactQueryClient(<TodoList />)

  // wait for data to load
  expect(await screen.findAllByRole('checkbox')).toHaveLength(TASKS_DATA.length)

  // delete first todo
  const deleteButton = screen.getAllByRole('button', { name: /Delete/i })[0]
  userEvent.click(deleteButton)
})
