import { AddNewTodoForm } from './AddNewTodoForm'
import { TodoLists } from 'app/TodoList/TodoLists'

export default function TodoList() {
  return (
    <>
      <h1 className="text-3xl text-gray-500 mb-10">
        <span className="font-extrabold text-indigo-600">Awesome</span>{' '}
        <span className="line-through">Todo List</span>
      </h1>

      <AddNewTodoForm />

      <TodoLists />
    </>
  )
}
