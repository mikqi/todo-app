import { useQueryClient } from '@tanstack/react-query'
import { Input } from 'components/Input'
import { Button } from 'components/Button'

import { useAddNewTask } from 'apis/todos'

export const AddNewTodoForm = () => {
  const queryClient = useQueryClient()
  const { mutate: addNewTask } = useAddNewTask()

  const handleSubmit = (e) => {
    e.preventDefault()
    const input = e.target.elements.todo

    if (input.value.trim()) {
      const newTask = {
        id: Date.now(),
        text: input.value,
        completed: false,
      }

      addNewTask(newTask, {
        onSuccess: (newTodo) => {
          input.value = ''

          // Snapshot the previous value
          const previousTodos = queryClient.getQueryData(['tasks'])

          // Optimistically update to the new value
          queryClient.setQueryData(['tasks'], () => {
            return [...previousTodos, newTodo]
          })
        },
      })
    }
  }

  return (
    <form className="flex gap-4" onSubmit={handleSubmit}>
      <Input type="text" placeholder="Add new todo" name="todo" />
      <Button type="submit" variant="primary">
        Submit
      </Button>
    </form>
  )
}
