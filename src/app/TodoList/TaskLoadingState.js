import { Shimmer } from 'components/Shimmer'

const WIDTH_DATA = [100, 120, 160, 200, 220]

const TOTAL_SHIMMER = new Array(5).fill('')

export const TaskLoadingState = () => {
  return (
    <>
      {TOTAL_SHIMMER.map((_shimmer, idx) => (
        <div key={idx} className="flex py-4 gap-4">
          <Shimmer rounded width={24} height={24} />
          <Shimmer
            height={22}
            width={WIDTH_DATA[Math.floor(Math.random() * WIDTH_DATA.length)]}
          />
        </div>
      ))}
    </>
  )
}
