import { useQueryClient } from '@tanstack/react-query'
import { TaskLoadingState } from 'app/TodoList/TaskLoadingState'
import { Todo } from 'components/Todo'

import {
  useFetchTasks,
  useDeleteTaskById,
  useToggleTaskCompleted,
} from 'apis/todos'

export const TodoLists = () => {
  const { data: tasks, status } = useFetchTasks()
  const { mutate: deleteTask } = useDeleteTaskById()
  const { mutate: toggleTaskCompleted } = useToggleTaskCompleted()
  const queryClient = useQueryClient()

  const handleDeleteTask = (id) => {
    deleteTask(id, {
      onSuccess: async () => {
        await queryClient.cancelQueries(['tasks'])

        // Optimistically update to the new value
        queryClient.setQueryData(['tasks'], (old) =>
          old.filter((task) => task.id !== id),
        )
      },
    })
  }

  const handleCheckboxChange = (id, completed) => {
    toggleTaskCompleted(
      {
        id,
        completed: !completed,
      },
      {
        onSuccess: async (newTodo) => {
          await queryClient.cancelQueries(['tasks'])

          // Snapshot the previous value
          const previousTodos = queryClient.getQueryData(['tasks'])

          // Optimistically update to the new value
          queryClient.setQueryData(['tasks'], () => {
            return previousTodos.map((todo) => {
              if (todo.id === id) {
                return newTodo
              }
              return todo
            })
          })
        },
      },
    )
  }

  return (
    <div className="mt-4 divide-y divide-gray-200 border-t border-b border-gray-200">
      {status === 'loading' && (
        <div data-testid="loading">
          <TaskLoadingState />
        </div>
      )}
      {status === 'success' &&
        tasks?.map((task) => (
          <Todo
            key={task.id}
            label={task.text}
            checked={task.completed}
            onChange={() => {
              handleCheckboxChange(task.id, task.completed)
            }}
            onDelete={() => handleDeleteTask(task.id)}
          />
        ))}
    </div>
  )
}
