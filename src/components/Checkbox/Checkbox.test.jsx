import { render, fireEvent, screen } from '@testing-library/react'
import { Checkbox } from 'components/Checkbox'

test('Checkbox: should render properly', () => {
  render(<Checkbox />)
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
})

test('Checkbox: should render properly with checked', () => {
  render(<Checkbox defaultChecked={true} />)
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toHaveAttribute('checked')
})

test('Checkbox: should render properly with disabled', () => {
  render(<Checkbox disabled />)
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toHaveAttribute('disabled')
})

test('Checkbox: should render properly with onChange', () => {
  const onChange = jest.fn()
  render(<Checkbox onChange={onChange} />)
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  fireEvent.click(screen.getByRole('checkbox'))
  expect(onChange).toHaveBeenCalled()
})

test('Checkbox: should render properly with rounded', () => {
  render(<Checkbox rounded />)
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toHaveClass('rounded-full')
})
