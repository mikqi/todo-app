import clsx from 'clsx'
import PropTypes from 'prop-types'

/**
 * @param {React.HTMLAttributes<HTMLInputElement>} props
 */
export const Checkbox = ({ rounded = false, ...props }) => {
  const classes = clsx(
    'h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500',
    rounded && 'rounded-full h-6 w-6',
  )
  return <input type="checkbox" className={classes} {...props} />
}

Checkbox.propTypes = {
  /**
   * Rounded checkbox style (default: false)
   */
  rounded: PropTypes.bool,
}

Checkbox.displayName = 'Checkbox'
