import clsx from 'clsx'
import PropTypes from 'prop-types'

/**
 * @param {React.HTMLAttributes<HTMLInputElement>} props
 */
export const Input = ({ className, ...props }) => {
  const classes = clsx(
    'block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm',
    className,
  )
  return <input className={classes} {...props} />
}

Input.propTypes = {
  className: PropTypes.string,
}
