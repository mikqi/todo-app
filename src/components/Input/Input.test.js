import { render, fireEvent, screen } from '@testing-library/react'
import { Input } from 'components/Input'

test('Input: should render properly', () => {
  render(<Input />)
  expect(screen.getByRole('textbox')).toBeInTheDocument()
})

test('Input: should render properly with placeholder', () => {
  render(<Input placeholder="Enter your name" />)
  expect(screen.getByRole('textbox')).toBeInTheDocument()
  expect(screen.getByRole('textbox')).toHaveAttribute(
    'placeholder',
    'Enter your name',
  )
})

test('Input: should render properly with value', () => {
  render(<Input defaultValue="John Doe" />)
  expect(screen.getByRole('textbox')).toBeInTheDocument()
  expect(screen.getByRole('textbox')).toHaveAttribute('value', 'John Doe')
})

test('Input: should render properly with onChange', () => {
  const onChange = jest.fn()
  render(<Input onChange={onChange} />)
  expect(screen.getByRole('textbox')).toBeInTheDocument()
  fireEvent.change(screen.getByRole('textbox'), {
    target: { value: 'John Doe' },
  })
  expect(onChange).toHaveBeenCalled()
})
