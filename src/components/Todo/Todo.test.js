import { render, screen, fireEvent } from '@testing-library/react'
import { Todo } from 'components/Todo'

test('renders a todo', () => {
  render(<Todo label="Add an apple" />)
  const todoElement = screen.getByText(/Add an apple/i)
  expect(todoElement).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
})

test('renders a todo with children', () => {
  render(<Todo>Add an apple</Todo>)
  const todoElement = screen.getByText(/Add an apple/i)
  expect(todoElement).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
})

test('renders a todo with checked', () => {
  render(<Todo label="Add an apple" checked />)
  const todoElement = screen.getByText(/Add an apple/i)
  expect(todoElement).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toHaveAttribute('checked')
})

test('renders a todo with event click on label toggle line through label', () => {
  render(<Todo label="Add an apple" />)
  const todoElement = screen.getByText(/Add an apple/i)
  expect(todoElement).toBeInTheDocument()
  expect(todoElement).not.toHaveClass('line-through')
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).not.toHaveAttribute('checked')

  fireEvent.click(todoElement)
  expect(screen.getByRole('checkbox')).toHaveAttribute('checked')
  expect(todoElement).toHaveClass('line-through')
})

test('renders a todo with event click on label should trigger onChange', () => {
  const onChange = jest.fn()
  render(<Todo label="Add an apple" onChange={onChange} />)
  const todoElement = screen.getByText(/Add an apple/i)
  expect(todoElement).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).not.toHaveAttribute('checked')

  fireEvent.click(todoElement)
  expect(screen.getByRole('checkbox')).toHaveAttribute('checked')
  expect(onChange).toHaveBeenCalled()
})

test('renders a todo with event click on delete button should trigger onDelete', () => {
  const onDelete = jest.fn()
  render(<Todo label="Add an apple" onDelete={onDelete} />)
  const todoElement = screen.getByText(/Add an apple/i)
  expect(todoElement).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).toBeInTheDocument()
  expect(screen.getByRole('checkbox')).not.toHaveAttribute('checked')

  fireEvent.click(screen.getByRole('button'))
  expect(onDelete).toHaveBeenCalled()
})
