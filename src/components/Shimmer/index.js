import clsx from 'clsx'
import PropTypes from 'prop-types'
import 'components/Shimmer/Shimmer.css'

export const Shimmer = ({
  height = 16,
  width = 100,
  style = {},
  rounded,
  className,
  noOfLines = 1,
  ...props
}) => {
  const classes = clsx('shimmer', rounded && 'rounded', className)
  const lines = new Array(noOfLines).fill('')

  return (
    <>
      {lines.map((_line, idx) => (
        <div
          className={classes}
          style={Object.assign({}, { width, height }, { ...style })}
          {...props}
          key={idx}
        />
      ))}
    </>
  )
}

Shimmer.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
  rounded: PropTypes.bool,
  style: PropTypes.object,
  className: PropTypes.string,
  noOfLines: PropTypes.number,
}

Shimmer.displayName = 'Shimmer'
