import { render, screen } from '@testing-library/react'
import { Shimmer } from 'components/Shimmer'

test('Shimmer: should render properly', () => {
  render(<Shimmer data-testid="shimmer" />)
  expect(screen.getByTestId('shimmer')).toBeInTheDocument()
})

test('Shimmer: should render properly with height', () => {
  render(<Shimmer data-testid="shimmer" height={100} />)
  expect(screen.getByTestId('shimmer')).toBeInTheDocument()
  expect(screen.getByTestId('shimmer')).toHaveStyle('height: 100px')
})

test('Shimmer: should render properly with width', () => {
  render(<Shimmer data-testid="shimmer" width={100} />)
  expect(screen.getByTestId('shimmer')).toBeInTheDocument()
  expect(screen.getByTestId('shimmer')).toHaveStyle('width: 100px')
})

test('Shimmer: should render properly with rounded', () => {
  render(<Shimmer data-testid="shimmer" rounded />)
  expect(screen.getByTestId('shimmer')).toBeInTheDocument()
  expect(screen.getByTestId('shimmer')).toHaveClass('rounded')
})

test('Shimmer: should render properly with className', () => {
  render(<Shimmer data-testid="shimmer" className="test-class" />)
  expect(screen.getByTestId('shimmer')).toBeInTheDocument()
  expect(screen.getByTestId('shimmer')).toHaveClass('test-class')
})

test('Shimmer: should render properly with noOfLines', () => {
  render(<Shimmer data-testid="shimmer" noOfLines={3} />)
  expect(screen.getAllByTestId('shimmer')[0]).toBeInTheDocument()
  expect(screen.getAllByTestId('shimmer')).toHaveLength(3)
})
