import { render, fireEvent, screen } from '@testing-library/react'
import { Button } from 'components/Button'

test('Button: should render properly', () => {
  render(<Button>Click me</Button>)
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
})

test('Button: should render properly with left icon', () => {
  render(<Button leftIcon={() => <span>Left</span>}>Click me</Button>)
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
  expect(screen.getByText('Left')).toBeInTheDocument()
})

test('Button: should render properly with right icon', () => {
  render(<Button rightIcon={() => <span>Right</span>}>Click me</Button>)
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
  expect(screen.getByText('Right')).toBeInTheDocument()
})

test('Button: should render properly with left and right icon', () => {
  render(
    <Button
      leftIcon={() => <span>Left</span>}
      rightIcon={() => <span>Right</span>}
    >
      Click me
    </Button>,
  )
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
  expect(screen.getByText('Left')).toBeInTheDocument()
  expect(screen.getByText('Right')).toBeInTheDocument()
})

test('Button: should render properly with variant', () => {
  render(<Button variant="danger">Click me</Button>)
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
  expect(screen.getByRole('button')).toHaveClass('bg-red-100')
})

test('Button: should render properly with size', () => {
  render(<Button size="xs">Click me</Button>)
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
  expect(screen.getByRole('button')).toHaveClass('px-2.5 py-1.5 text-xs')
})

test('Button: should render properly with label', () => {
  render(<Button label="Click me" />)
  expect(screen.getByRole('button')).toBeInTheDocument()
  expect(screen.getByText('Click me')).toBeInTheDocument()
})

test('Button: should render properly with onClick', () => {
  const onClick = jest.fn()
  render(<Button onClick={onClick}>Click me</Button>)
  fireEvent.click(screen.getByRole('button'))
  expect(onClick).toHaveBeenCalled()
})
