import axios from 'axios'

import { useQuery, useMutation } from '@tanstack/react-query'

const API_URL = 'http://localhost:3001/tasks'

export function useFetchTasks() {
  return useQuery(['tasks'], async () => {
    try {
      const response = await axios.get(API_URL)
      return response.data
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
  })
}

export function useAddNewTask() {
  return useMutation(async (task) => {
    try {
      const response = await axios.post(API_URL, task)
      return response.data
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
  })
}

export function useDeleteTaskById() {
  return useMutation(async (id) => {
    try {
      const response = await axios.delete(`${API_URL}/${id}`)
      return response.data
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
  })
}

export function useToggleTaskCompleted() {
  return useMutation(async (data) => {
    try {
      const response = await axios.patch(`${API_URL}/${data.id}`, {
        completed: data.completed,
      })
      return response.data
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
  })
}
