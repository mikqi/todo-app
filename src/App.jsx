import TodoList from 'app/TodoList'

export default function App() {
  return (
    <div className="bg-gray-100 ">
      <div className="max-w-md m-auto p-4 bg-white min-h-screen shadow-xl">
        <TodoList />
      </div>
    </div>
  )
}
