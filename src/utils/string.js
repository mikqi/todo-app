/**
 * transform string to snake case
 * @param {string} str - The string to be snake cased
 * @returns string
 */
export const toSnakeCase = (str) => {
  return str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1_$2').toLowerCase()
}
