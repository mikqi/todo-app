import { toSnakeCase } from 'utils/string'

test('toSnakeCase should work properly', () => {
  expect(toSnakeCase('helloWorld')).toBe('hello_world')
  expect(toSnakeCase('hello World')).not.toBe('hello_world')
  expect(toSnakeCase('helloWorld')).not.toBe('helloWorld')
  expect(toSnakeCase('helloWorld')).not.toBe('hello-world')
})
